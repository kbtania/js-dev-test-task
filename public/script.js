// buttons
const openFormBtn = document.querySelector('.popup-btn');
const popup = document.querySelector('.popup');
const closeBtn = document.querySelector('.close');
const submitBtn = document.querySelector('.submit-btn');

// fields
const nameInput = document.getElementById('name');
const emailInput = document.getElementById('email');
const phoneInput = document.getElementById('phone');

const errorAlert = document.querySelector('.popup__error');
const result = document.querySelector('.result');

// close popup
closeBtn.addEventListener('click', () => {
    popup.style.display = 'none';
})

// open popup
openFormBtn.addEventListener('click', () => {
    result.style.display = 'none';
    popup.style.display = 'block';
})

let dataToSend = {
    name: '',
    email: '',
    phone: ''
}

nameInput.addEventListener('keyup', () => {
    const nameError = document.querySelector('.name-error')
    if (nameInput.value.length <= 2) {
        nameError.innerHTML = 'Name value must be more than 2 symbols';
        nameError.style.display = 'block';
    } else {
        nameError.style.display = 'none';
        dataToSend.name = nameInput.value;
    }
})

emailInput.addEventListener('keyup', () => {
    const emailError = document.querySelector('.email-error')
    if (emailInput.value.length <= 2) {
        emailError.innerHTML = 'Email value must be more than 2 symbols';
        emailError.style.display = 'block';
    } else {
        emailError.style.display = 'none';
        dataToSend.email = emailInput.value;
    }
})

phoneInput.addEventListener('keyup', () => {
    const phoneError = document.querySelector('.phone-error');
    if (phoneInput.value.length <= 2) {
        phoneError.innerHTML = 'Phone value number must be more than 2 symbols';
        phoneError.style.display = 'block';
    } else {
        phoneError.style.display = 'none';
        dataToSend.phone = phoneInput.value;
    }
})

// submit
submitBtn.addEventListener('click', (e) => {
    e.preventDefault();
    if (nameInput.value.length <= 2 || emailInput.value.length <= 2 || phoneInput.value.length <= 2) {
        errorAlert.innerHTML = 'Please check the length of each field!';
        errorAlert.style.display = 'block';
        setTimeout(() => {
            errorAlert.style.display = 'none';
        }, 2000)
    } else {
        console.log(dataToSend);
        popup.style.display = 'none';
        result.style.display = 'block';
        nameInput.value = '';
        emailInput.value = '';
        phoneInput.value = '';
    }
})


